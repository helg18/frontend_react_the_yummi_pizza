import React, {Component} from 'react';
import Body from "./components/layouts/Body/Body";
import Footer from "./components/layouts/Footer/Footer";

class App extends Component {
  render() {
    return (
      <div className="hero is-fullheight">
        <Body />
        <Footer />
      </div>
    );
  }
}

export default App;
