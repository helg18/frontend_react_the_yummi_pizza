import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'toastr2/dist/toastr.min.css';
import 'bulma/css/bulma.min.css'
import './index.css';
import App from './App';
import store from "./redux/store";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
