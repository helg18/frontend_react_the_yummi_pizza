import React, {Component} from 'react';
import { connect } from 'react-redux';
import {withRouter} from "react-router-dom";

class ProductList extends Component {

  render() {
    return (
      <div className="column is-9">
        <p className="title is-4 has-text-grey">Availables pizzas</p>
        {
          this.props.products.map((product)=>{
            return (
              <article className="media" key={product.id} style={{marginBotton:'10px'}}>
                <figure className="media-left">
                  <p className="image is-128x128">
                    <img src={product.img_url} alt={product.name} />
                  </p>
                </figure>
                <div className="media-content">
                  <div className="content">
                    <p>
                      <strong>{product.name}</strong> <small>@johnsmith</small> <small>31m</small>
                      <br />
                    </p>
                    <p>
                      <button className="button is-info">Add to cart</button>
                    </p>
                  </div>
                </div>
              </article>);
          })
        }
      </div>
    );
  }

}

const mapStateToProps = state => ({
  pricesOn: state.pricesOn,
  products: state.products
})

const mapDispatchToProps = dispatch => ({})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductList));
