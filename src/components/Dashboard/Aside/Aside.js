import React, {Component} from 'react';
import {Link} from "react-router-dom";

class Aside extends Component {
  render() {
    return (
      <div className="column is-3 ">
        <aside className="menu is-hidden-mobile">
          <p className="menu-label">
            General
          </p>
          <ul className="menu-list">
            <li><Link to={'/dashboard'} className="is-active">Dashboard</Link></li>
            <li><Link to={'/neworder'}>New Order</Link></li>
          </ul>
          <p className="menu-label">
            Administration
          </p>
          <ul className="menu-list">
            <li><Link to={'/myaddresses'}>My Addresses</Link></li>
            <li>
              <Link to={'/myorders'} className="menu-item">My orders</Link>
            </li>
          </ul>
        </aside>
      </div>
    );
  }
}

export default Aside;
