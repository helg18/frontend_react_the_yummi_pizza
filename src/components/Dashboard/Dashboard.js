import React, {Component} from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom'
import './Dashboard.css';
import Aside from "./Aside/Aside";
import Main from "./Main/Main";

class Dashboard extends Component {
  constructor(props) {
    super(props);

    // validate logged user
    if (!this.props.logged){
      let { history } = this.props
      history.push('/login');
    }
  }
  printChild = () => {
    if (this.props.location.pathname === '/dashboard'){
      return <Main parentProp={this.props} />;
    }
  }

  render() {
    return (
      <div className="dashboard">
        <br/>
        <div className="container">
          <div className="columns">
            <Aside />
            {
              this.printChild()
            }
        </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  addresses : state.addresses,
  logged: state.logged,
  orders : state.orders,
  pricesOn: state.pricesOn,
  shoppingCart: state.shoppingCart,
  user : state.user
})

const mapDispatchToProps = dispatch => ({})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Dashboard));
