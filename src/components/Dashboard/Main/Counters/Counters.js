import React, {Component} from 'react';

class Counters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countAddress: this.props.countAddress,
      countOrder: this.props.countOrder,
      memberSince: this.props.user.created_at
    }

  }

  memberSince = () => {
    if (this.state.memberSince !== null) {
      let date = new Date(this.state.memberSince);
      return date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
    }
    return "-";
  }

  render() {
    return (
      <section className="info-tiles">
        <div className="tile is-ancestor has-text-centered">
          <div className="tile is-parent">
            <article className="tile is-child box">
              <p className="title">{this.state.countAddress}</p>
              <p className="subtitle">Addresses</p>
            </article>
          </div>
          <div className="tile is-parent">
            <article className="tile is-child box">
              <p className="title">{this.state.countOrder}</p>
              <p className="subtitle">Orders</p>
            </article>
          </div>
          <div className="tile is-parent">
            <article className="tile is-child box">
              <p className="title">{this.memberSince()}</p>
              <p className="subtitle">Member since</p>
            </article>
          </div>
        </div>
      </section>
    );
  }
}

export default Counters;
