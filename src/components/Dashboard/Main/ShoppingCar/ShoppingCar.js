import React, {Component} from 'react';
import {Link} from "react-router-dom";

class ShoppingCar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      shoppingCart: this.props.shoppingCart,
      pricesOn: this.props.pricesOn
    }
  }
  render() {
    return (
      <div className="card events-card">
        <header className="card-header">
          <p className="card-header-title">
            Shopping Car
          </p>
        </header>
        <div className="card-table">
          <div className="content">
            <table className="table is-fullwidth is-striped">
              <tbody>
                { this.getShoppingCarContent() }
              </tbody>
            </table>
          </div>
        </div>
        <footer className="card-footer">
          <Link to={'/shoppingcar'} className="card-footer-item">View All</Link>
        </footer>
      </div>
    );
  }
  getShoppingCarContent = () => {
    let car = this.state.shoppingCart;
    if (car.length <= 0){
      return (<tr>
        <td>Your car is empty!</td>
      </tr>)

    } else {
      let items = car.map(product => {
        return (<tr key={product.id}>
          <td width="5%">{product.id}</td>
          <td>{this.showPrice(product)}</td>
          <td>{product.product}</td>
          <td className="level-right"><button className="button is-small is-danger">X</button></td>
        </tr>)

      })
      return items;
    }
  }

  showPrice = (product) => {
    if (this.state.pricesOn === 'usd') {
      return "$ " + product.price_usd;
    }
    if (this.state.pricesOn === 'eur') {
      return "€ " + product.price_eur
    }
    return 'invalid';
  }

}

export default ShoppingCar;
