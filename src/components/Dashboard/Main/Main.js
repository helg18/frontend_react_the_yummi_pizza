import React, {Component} from 'react';
import HelloUser from "./HelloUser/HelloUser";
import Counters from "./Counters/Counters";
import ShoppingCar from "./ShoppingCar/ShoppingCar";
import OrderList from "../../Order/OrderList/OrderList";

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countAddress : this.props.parentProp.addresses.length,
      countOrders : this.props.parentProp.orders.length,
      orders: this.props.parentProp.orders,
      pricesOn: this.props.parentProp.pricesOn,
      shoppingCart: this.props.parentProp.shoppingCart,
      user: this.props.parentProp.user
    }
  }

  render() {
    return (
      <div className="column is-9">
        <HelloUser userName={this.state.user.name} />
        <Counters
          countAddress={this.state.countAddress}
          countOrder={this.state.countOrders}
          user={this.state.user}
        />
        <div className="columns">
          <div className="column is-6">
            <ShoppingCar
              pricesOn={this.state.pricesOn}
              shoppingCart={this.state.shoppingCart}
            />
          </div>
          <div className="column is-6">
            <OrderList
              orders={this.state.orders}
              pricesOn={this.state.pricesOn}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Main;
