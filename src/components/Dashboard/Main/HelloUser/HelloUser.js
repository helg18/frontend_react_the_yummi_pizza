import React, {Component} from 'react';

class HelloUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: this.props.userName
    }
  }

  render() {
    return (
      <section className="hero is-info welcome is-small">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">
              Hello, {this.state.userName}.
            </h1>
            <h2 className="subtitle">
              I hope you are having a great day!
            </h2>
          </div>
        </div>
      </section>
    );
  }
}

export default HelloUser;
