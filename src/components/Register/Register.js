import React, {Component} from 'react';
import { connect } from 'react-redux';
import Api from "../../services/axiosService";
import Toastr from 'toastr2';

class Register extends Component {
  toastr = new Toastr();

  constructor(props) {
    super(props);

    // validate logged user
    if (this.props.logged){
      this.toastr.success("Already you are logged");
      let { history } = this.props
      history.push('/dashboard');
    }
  }

  render() {
    return (
      <div className="hero-body ">
        <div className="container has-text-centered has-background-light">
          <div className="column">
            <div className="container has-text-centered">
              <div className="column is-4 is-offset-4">
                <h3 className="title has-text-black">Register</h3>
                <div className="box">
                  <form onSubmit={this.handleRegister}>
                    <div className="field">
                      <div className="control">
                        <input
                          className="input is-large"
                          type="text"
                          name="name"
                          placeholder="Your Full Name"
                          autoFocus=""
                          autoComplete="off"
                          required
                        />
                      </div>
                    </div>
                    <div className="field">
                      <div className="control">
                        <input
                          className="input is-large"
                          type="email"
                          name="email"
                          placeholder="Your Email"
                          autoFocus=""
                          autoComplete="off"
                          required
                        />
                      </div>
                    </div>
                    <div className="field">
                      <div className="control">
                        <input
                          className="input is-large"
                          type="password"
                          name="password"
                          placeholder="Your Password"
                          required
                        />
                      </div>
                    </div>
                    <div className="field">
                      <div className="control">
                        <input
                          className="input is-large"
                          type="password"
                          name="confirm_password"
                          placeholder="Confirm your Password"
                          required
                        />
                      </div>
                    </div>
                    <button className="button is-block is-info is-large is-fullwidth">
                      Register <i className="fa fa-sign-in" aria-hidden="true"></i>
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  handleRegister = async (e) => {
    e.preventDefault();
    const credentials = {
      name: e.target.name.value,
      email: e.target.email.value,
      password: e.target.password.value,
      confirm_password: e.target.confirm_password.value
    }
    let apiToken = await Api.registerUser(credentials);
    if (apiToken) {
      // set session
      this.props.registerUser(apiToken);

      // get info user logged
      let user = await Api.getUser();

      // set User Logged
      this.props.setUserLogged(user);

      // eslint-disable-next-line react/prop-types
      let {history} = this.props;
      // eslint-disable-next-line react/prop-types
      history.push('/dashboard');
    }
  }
}


const mapStateToProps = state => ({
  addresses : state.addresses,
  logged: state.logged,
  apiToken : state.apiToken,
  orders : state.orders,
  user : state.user,
})

const mapDispatchToProps = dispatch => ({
  registerUser(apiToken) {
    dispatch({
      type: "LOGIN_USER",
      apiToken
    });
  },
  setUserLogged(user) {
    dispatch({
      type: "SET_ALL_PROPS",
      addresses: user.addresses,
      orders: user.orders,
      user: user
    })
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Register);
