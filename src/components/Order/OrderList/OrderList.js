import React, {Component} from 'react';
import {Link} from "react-router-dom";

class OrderList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      orders: this.props.orders,
      pricesOn: this.props.pricesOn
    }
  }

  render() {
    return (
      <div className="card events-card">
        <header className="card-header">
          <p className="card-header-title">
            Order List
          </p>
        </header>
        <div className="card-table">
          <div className="content">
            <table className="table is-fullwidth is-striped">
              <tbody>
              { this.getOrderList() }
              </tbody>
            </table>
          </div>
        </div>
        <footer className="card-footer">
          <Link to={'/myorders'} className="card-footer-item">View All</Link>
        </footer>
      </div>
    );
  }

  billedAt = (order) => {
      let date = new Date(order.created_at);
      return date.getDate()+'/'+date.getMonth()+'/'+date.getFullYear()+" -- "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
  }

  getOrderList = () => {
    let orders = this.state.orders;
    if (orders.length <= 0){
      return (<tr>
        <td>You have not orders still.</td>
      </tr>)

    } else {
      let items = orders.map(o => {
        return (<tr key={o.id}>
          <td width="5%">{o.id}</td>
          <td>{this.showPrice(o)}</td>
          <td>{this.billedAt(o)}</td>
          <td className="level-right"><button className="button is-small is-info">Open</button></td>
        </tr>)

      })
      return items;
    }
  }

  showPrice = (order) => {
    console.log(order)
    if (this.state.pricesOn === 'usd') {
      return "$ " + order.total_amount_usd;
    }
    if (this.state.pricesOn === 'eur') {
      return "€ " + order.total_amount_eur
    }
    return 'invalid';
  }
}

export default OrderList;
