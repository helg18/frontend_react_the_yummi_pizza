import React, {Component} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Login from "../../Login/Login";
import Register from "../../Register/Register";
import Home from "../../Home/Home";
import Header from "../Header/Header";
import OrderList from "../../Order/OrderList/OrderList";
import Order from "../../Order/Order";
import ProductList from "../../Dashboard/Product/ProductList/ProductList";
import Main from "../../Dashboard/Main/Main";
import Dashboard from "../../Dashboard/Dashboard";

class Body extends Component {
  render() {
    return (
      <Router className="hero-body">
        <Header />
        <Switch>
          <Route exact path="/dashboard" >
            <Dashboard children={<Main/>} />
          </Route>
          <Route exact path="/neworder" >
            <Dashboard>
              <ProductList parentProps={this.props}/>
            </Dashboard>
          </Route>
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/myorders">
            <OrderList />
          </Route>
          <Route exact path="/order">
            <Order />
          </Route>
          <Route exact path="/products">
            <ProductList />
          </Route>
          <Route exact path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    );
  }
}

export default Body;
