import React, {Component} from 'react';
import {Link} from "react-router-dom";
import './Header.css'
import {connect} from 'react-redux'

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logged: this.props.logged
    }
  }

  render() {
    return (
      <nav className="navbar" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
          <Link to={'/'} className="navbar-item">
            <img src="https://cdn.pixabay.com/photo/2014/09/24/00/36/pizza-458400_960_720.png" width="82" height="28" alt="The Yummi Pizza"/>
          </Link>

          <div role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false"
             data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </div>
        </div>

        <div id="navbarBasicExample" className="navbar-menu">
          <div className="navbar-start">
            <Link className="navbar-item" to={'/'}>
              Home
            </Link>

            {
              this.props.logged ?
                <Link className="navbar-item" to={'/dashboard'}>
                  Dashboard
                </Link>
                : <div></div>
            }

            {
              this.props.logged ?
                <Link className="navbar-item" to={'/order'}>
                  New Order
                </Link>
                : <div></div>
            }

            {
              this.props.logged ?
                <div className="navbar-item has-dropdown is-hoverable">
                  <div className="navbar-link">
                    More
                  </div>

                  <div className="navbar-dropdown ">
                    <Link className="navbar-item" to={"/myorders"}>
                      My orders
                    </Link>
                    <Link className="navbar-item" to={"/products"}>
                      Products
                    </Link>
                    <hr className="navbar-divider" />
                    <a className="navbar-item" href="emailto:helg18@gmail.com" target="_blank" rel="noopener noreferrer">
                      Contact by email
                    </a>
                  </div>
                </div>
                : <div></div>
            }

          </div>

          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                {
                  !this.props.logged ?
                    <Link to={'/register'} className="button is-primary">
                      <strong>Sign up</strong>
                    </Link>
                    : <div></div>
                }
                {
                  !this.props.logged ?
                    <Link to={'/login'} className="button is-light">
                      Log in
                    </Link>
                    : <div></div>
                }
                {
                  this.props.logged ?
                    <button className="button is-light">
                      Logout
                    </button>
                    : <div></div>
                }

              </div>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

const mapStateToProps = state => ({
  logged: state.logged
})

const mapDispatchToProps = dispatch => ({
  registerUser(apiToken) {
    dispatch({
      type: "LOGIN_USER",
      apiToken
    });
  },
  setUserLogged(user) {
    dispatch({
      type: "SET_ALL_PROPS",
      addresses: user.addresses,
      orders: user.orders,
      user: user
    })
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Header);
