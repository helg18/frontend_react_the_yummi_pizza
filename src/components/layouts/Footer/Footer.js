import React, {Component} from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="hero-foot">
        <div className="has-text-grey-light is-fixed-bottom has-text-centered">
          <span>This app was a test provided by <a href="https://innoscripta.com/" target="_blank" rel="noopener noreferrer">Innoscripta.com</a></span><br/>
          <span>Developed by <a href="mailto:helg18@gmail.com" target="_blank" rel="noopener noreferrer">Henry Leon</a></span>
        </div>
      </footer>
    );
  }
}

export default Footer;
