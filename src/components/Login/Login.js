import React, {Component} from 'react';
import Api from "../../services/axiosService";
import { connect } from 'react-redux';
import Toastr from 'toastr2';

class Login extends Component {
  toastr = new Toastr();

  constructor(props) {
    super(props);

    this.state = {
      logged: this.props.logged
    }

    // validate logged user
    if (this.state.logged){
      this.toastr.success("Already you are logged");
      let { history } = this.props
      history.push('/dashboard');
    }
  }

  printButtonIfNotLogged = () => {
    return (
      <div className="hero-body ">
        <div className="container has-text-centered has-background-light">
          <div className="column">
            <div className="container has-text-centered">
              <div className="column is-4 is-offset-4">
                <h3 className="title has-text-black">Login</h3>
                <p className="subtitle has-text-black top ">Please login to proceed.</p>
                <div className="box">
                  <form onSubmit={this.handleLogin}>
                    <div className="field">
                      <div className="control">
                        <input
                          autoComplete="off"
                          autoFocus=""
                          className="input is-large"
                          name="email"
                          placeholder="Your Email"
                          required
                          type="email"
                        />
                      </div>
                    </div>

                    <div className="field">
                      <div className="control">
                        <input
                          className="input is-large"
                          name="password"
                          placeholder="Your Password"
                          required
                          type="password"
                        />
                      </div>
                    </div>
                    <button className="button is-block is-info is-large is-fullwidth">
                      Login <i className="fa fa-sign-in" aria-hidden="true"></i>
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    if (this.state.logged){
      return <div></div>;
    }
    return this.printButtonIfNotLogged();

  }

  handleLogin = async (e) => {
    e.preventDefault();
    const credentials = {
      email: e.target.email.value,
      password: e.target.password.value
    }
    let apiToken = await Api.loginUser(credentials);
    if (apiToken) {
      // set session
      this.props.loggedUser(apiToken);

      // get info user logged
      let user = await Api.getUser();

      // set User Logged
      this.props.setUserLogged(user);

      // get Products
      let products = await Api.getProducts();

      // get Products
      this.props.setProducts(products);


      // eslint-disable-next-line react/prop-types
      let { history } = this.props;
      // eslint-disable-next-line react/prop-types
      history.push('/dashboard');
    }
  }
}

const mapStateToProps = state => ({
  addresses : state.addresses,
  logged: state.logged,
  apiToken : state.apiToken,
  orders : state.orders,
  user : state.user,
})

const mapDispatchToProps = dispatch => ({
  loggedUser(apiToken) {
    dispatch({
      type: "LOGIN_USER",
      apiToken
    });
  },
  setUserLogged(user) {
    dispatch({
      type: "SET_ALL_PROPS",
      addresses: user.addresses,
      orders: user.orders,
      user: user
    })
  },
  setProducts(products){
    dispatch({
      type: "SET_PRODUCTS",
      products
    });
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Login);
