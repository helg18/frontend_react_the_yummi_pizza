import React, {Component} from 'react';
import {Link} from "react-router-dom";

class Home extends Component {
  render() {
    return (
      <div className="hero-body ">
        <div className="container has-text-centered has-background-light">
          <div className="column">
            <Link to={'/login'}>Login</Link> or <Link to={'/register'}>Register</Link> <br/> to make a order.
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
