import { createStore } from "redux";

const INITIAL_STATE = {
  addresses: [],
  apiToken: '',
  logged: false,
  orders: [],
  pricesOn: 'usd',
  products: [],
  shippings: [],
  shoppingCart: [],
  user: []
}

const mainReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "LOGIN_USER":
      return {
        ...state,
        apiToken: action.apiToken,
        logged: true,
      }

    case "SET_ALL_PROPS":
      return {
        ...state,
        addresses: action.addresses,
        orders: action.orders,
        user: action.user
      }

    case "SET_PRODUCTS":
      return {
        ...state,
        products: action.products
      }

    case "SET_SHOPPINGCAR":
      return {
        ...state,
        shoppingCart: action.products
      }

    default:
      return state;
  }
}

export default createStore(mainReducer);
