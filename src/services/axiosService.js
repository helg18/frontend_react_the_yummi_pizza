import axios from 'axios/dist/axios';
import Toastr from 'toastr2';

class Api {
  toastr = new Toastr();

  request = axios.create({
    baseURL: 'https://www.pizzaback.henryleon.com.ve/api/v1/',
    withCredentials: true
  });

  // Register User
  registerUser = async (credentials) => await this.request.post('/register', credentials)
    .then(response => {
      if (response.data.error === true){
        let errors = Object.entries(response.data.message)
        errors.map(msg => {
          this.toastr.error(msg[1]);
          return false;
        })
      } else {
        this.request.defaults.headers.common['Authorization'] = 'Bearer '+response.data.data.token;
          this.toastr.success("Registered successful");
        return 'Bearer '+response.data.data.token;
      }
    })
    .catch((e)=>{
      this.toastr.error(e.message);
    })

  // Login User
  loginUser = async (credentials) => await this.request.post('/login', credentials)
    .then(response => {
      if (response.data.error === true){
        this.toastr.error(response.data.message);
        return false;
      } else {
        this.request.defaults.headers.common['Authorization'] = 'Bearer '+response.data.data.token;
        this.toastr.success("Login successful");
        return 'Bearer '+response.data.data.token;
      }
    })
    .catch((e)=>{
      this.toastr.error(e.message);
    })

  // Get Logged User
  getUser = async () => await this.request.get('/getUser')
    .then(response => {
      if (response.data.error === true) {
        this.toastr.error(response.data.message);
        return false;
      } else {
        return response.data.data
      }
    })
    .catch((e)=>{
      this.toastr.error(e.message);
    })

  getProducts = async () => await this.request.get('/product')
    .then(response => {
      if (response.data.error === true){
      let errors = Object.entries(response.data.message)
      errors.map(msg => {
        this.toastr.error(msg[1]);
        return false;
      })
    } else {
      return response.data.data
    }
    })
    .catch((e)=>{
      this.toastr.error(e.message);
    })

}

// Exporting a new Api object to be used
export default new Api();
